從奢華的馬車上，下到了街道。

白亞的車體與炫彩的金銀裝飾、馬蹄響鳴被牽引著的、是體格健壯的四頭白馬。那個周圍、穿著著美麗的鎧甲的士兵與騎馬的士兵並走著。在過一會兒、比先頭相比較為遜色的馬車數台追走著。

疾走馬車的樣子、被街道沿邊麥畑里的農夫當做珍物看著。就像是描繪的大貴族一樣。對缺乏娛樂、尋求刺激的飢餓平民來說、這是非常好的話題。
後面的會話、可能對馬車隨行的騎士進行稱讚、野可能是對自己的血液進行搾取獲得財富的貴族進行詛咒、反正都聽不到。

好奇心巨大的農夫、停下了手中的作業看著為馬車送別。對於沒有機會的平民來說、看到貴族的馬車的同樣稀有的事情。

在阿爾凱爾王國只要稍微懂得一點典禮的人都知道、白馬是給伯爵家以上的大貴族使用的。這是權威象徵的馬、不允許玷污的白色、是相當高位者才能使用的。

但是這不可能是公爵家的人。擁有王家血統的血宰相、元帥之位使用的、是稀少的靈獸獨角獸。之允許讓少女騎乘被謳歌為純潔的守護者的獨角獸、正是富貴的頂點的王之血統才能使用的。另外、王家與公爵家的區別是馬脖子是否有系上絲帶。當然、講究這種裝飾的是王家。

也就是說、那個馬車的主人是伯爵家到侯爵家的級別。從這裡要確定是哪一方的話、就只有從車體雕刻的紋章進行判別。這是意外的難事。因為全部貴族、都有自家獨有的家紋與自己獨自的紋章。要是把當主的兄弟兒子這些親族縁戚全部統計起來、那麼。紋章的總數將會是非常龐大的數量。發現細微的差異、認出紋章特地的主人可以說是一個非常花時間的技能。

那麼、就從先頭的豪壯馬車的紋章來看一下。

首先紋章是全體的四角方形盾。這是阿爾凱爾一般的類型。貴族的紋章大部分的設計、都是盾的形狀。那是在戰場的軍功防御的发願而來、用於盾來識別、也是防御魔法的刻印的說法、反正是與防具是同樣的意思。另外、聖加侖歴戰的勇者裡也有一小部分用盾做紋章、瑪爾貝亞是向下紡錘型、盾牌的形狀會隨著國家的不同而不同。看到紋章、就能夠辨別出是哪個國家的貴族。

那麼、那是阿爾凱爾的貴族。接下來。

是盾牌的顏色數量、以及什麼顏色。藝術的阿爾凱爾王國的貴族、對紋章的顏色也有講究。具體的來說王家與公爵家的紋章盾牌是三色還有一色是青色。侯爵家三色、代表藍色血液的王族象徵的青色、則是禁止使用。伯爵家二色。這以下一色。

那個紋章是赤黑二色。在盾牌的中央四面塗色。想來這是伯爵家的。

只要看盾牌刻上的象徵。就能知道這個家大概的家系。一句話說貴族的家系、屬於嫡流派生的庶流、全部都用一貫的象徵、是這個國家的習俗。然後色數與什麼色搭配這是家裡特定的習俗。

上面刻著的象徵，是咬尾的化為圓圈的蛇──銜尾蛇。

蛇這個動物，作為象徵性是很不可受歡迎的。沒有手腳、冷血、チラチラ地吐著舌頭卑賤的看著別人、毒有又對人有害的種類、對獵物進行丸呑的野蠻殘酷行為、令人討厭的理由非常多。另一方面、脫皮生長、也是身體長壽的寓意、在這樣聯想好意的解釋下。神話故事了也有智慧的象徵。在這種不受歡迎的情況下、選為作為一族的象徵是很少有的、這樣就縮小範圍了、。

使用這個特定的紋章的主人。用表示永續性於完全性的円環的蛇作為象的一族、屬於伯爵家的人。加之各種煩瑣的識別、當主只會是プレイン・コート。那個歐布尼爾伯爵家的現當主、使用這個紋章的不會是他人。

───

馬車裡充滿的是居心不好的沈默。車上之人看著窗外牧歌的風景、沒有因為為了排解無聊而進行會話、只是一味的沉默。就如同針刺在了某個洞上、感覺聲音就像是要炸裂了一樣的緊張感。這樣危險的沉默、隨著車內推移著。

車上坐著四人、作為男女明顯分開了。

首先是男性。穿著典雅衣裝的金髮貴公子、萊納斯・斯特萊茵・歐布尼爾伯爵。作為領主正趕往自己封地的旅途上。作為馬車所有者的年輕貴族、與他人乘坐馬車表現出僵硬的表情。有時、會在無聊之餘對著座席用手指頭敲幾下發出聲音、然後每次都使車內的人身體更僵硬。

在旁邊坐著青臉的、是他的執事。掌管家中諸事、與同行的主人坐在同樣的位置。近侍的家臣裡、因為是車內身分最低的、而低著頭。

對面的女性。是楚楚動人的穿著夏服打扮的年輕婦人、希莫娜・梅莉亞・歐布尼爾。去年嫁給萊納斯的新婚妻子、是理所當然與丈夫在車上的人。但是、這對夫婦的婚姻關係從以前就不好是公開的秘密。伴侶之間也很少對話、沒有像其他人一樣說著話題。實際上她、與家裡的傭人也不那麼好。因為她與家中忌諱的那個次男交往較好。

最後的女人。因為她，他的面子比以往更放異彩。

首先氣氛不同。同樣是僵硬的表情、但是質量不一樣。與萊納斯的緊張感而孕育住的沈默厭煩相對、這位女性連苦惱的樣子也沒有。如果說其他人像是被凍住了、她就像是自然的雕像一樣。

裝束也有些不同。伯爵夫婦是貴族著飾、執事則是穿著有袖子的禮服、女人卻穿著鎧甲。胸前也好具足也好、雙手到頭盔、即使精緻的描繪著各種紋樣、然而武裝就是武裝。腰間掛著細身的鋭劍。這是能夠馬上奔赴戰場的裝備、能夠女人即使是伯爵也比不上的高位貴族。

這樣聽起來、會讓人覺得是打扮非常識的、威嚴的猛女吧。但是、如果看到那眼神的話感覺就完全不同了。如同英姿颯爽一般、艷麗如同金線一般的單馬尾。白皙的臉讓人覺得怜悧而鋭利、如同危險的利刃一般在朝露中綻放著美色。

這位戰乙女就如同勇者的守護天使一樣如同神話中選拔出來的美女。
車內的空氣明明讓人不舒服、而且越來越惡化的時候、這位靜靜的天女確釋放出清浄的香氣、真是難以置信。

一方面、最初如此長時間沉默的原因、她也佔了一部分。總之、她的存在的構成要素、全部是異質中的異質。那壓倒性的氛圍、打扮、容貌⋯⋯⋯不過是好的意義還是壊的意義都難以無視、難以說出口。這之中、對於歐布尼爾家來說她是唯一的局外人。

這時候、看著連呼吸都覺得奇怪的女人、終於有人點燃導火索了。
是萊納斯。

「⋯⋯起床了嗎？」

對秀麗的臉發起苛責、對閉著眼睛的女人這樣說。對這陰險毒辣的話、希莫娜微微的皺眉。想著對同乗的客人也這樣。
女人對著不禮貌的萊納斯。慢慢的張開碧眼、讓人聯想到牽牛花開花一樣。

「這是我要說的話、歐布尼爾伯爵」

女性聲音低啞。但是、不像是不高興的樣子。這應該是她天生的嗓音。
覺得耳朵被深深打動的萊納斯、輕輕吹了吹鼻子。

「當然、伊麗莎小姐──」
「不要叫小姐」

伊麗莎、被這樣稱呼的女人鋭利的打斷了。萊納斯凍結了起來。
她把語氣稍微緩和了一下。

「這個身體、是作為歷史的王國騎士」
「──失禮、巴爾巴斯特卿」
「嗯，那就好了」

心滿意足的點頭後、伊麗莎再次閉上了眼睛。
年輕伯爵的表情，再次扭曲了。

「你、現在可以認真工作了嗎？」
「嗯？」
「萊納斯、不要說得那麼過分」

希莫娜看不下去而開口了、丈夫無視。

「從剛才開始就閉上眼睛默默不語。怎麼看都不是正經的警護。我想你在睡覺吧」
「啊啊、是睡著了哦？」
「んなっ！？」

意想不到的答覆，萊納斯開口便語塞了。
睡著了？在王國伯爵面前、作為護衛？非常識也要有個限度。希莫娜也吃驚的眨著眼、執事上衣的右肩也是一個不小心滑了下來。
面對今天有好幾次僵硬的萊納斯，伊麗莎若無其事的繼續說明。

「稍微、護衛任務中睡眠的時間有些少。在沒有襲擊的時候進行小睡。這是理所當然的」
「就、就是這樣！神經太大條了、目光不離隨時準備應付事態才是護衛該做的吧！？」
「那是基本中的基本。我的是應用篇。馬車的周圍都有人並走著。在車中我是沒有必要那樣子的。而且、就跟伯爵說的為了準備應付意想不到的事態、要養精蓄銳才是吧？」

再次不高興的閉上眼睛、郁悶的說著。只做必要的、除此之外、沒有必要進行護衛護。伊麗莎的言行、讓萊納斯無法反駁。

「最後、只要你打個招呼馬上就能起來。如果有襲擊聲音肯定不小。絶對起得來。完全沒問題。⋯⋯所以、這個時候最害怕的就是神經疲勞。像刺蝟一樣進行護衛是沒有意義的、伯爵」
「⋯⋯呼」

希莫娜像是無法忍耐的嘆氣。伊麗莎覺得不可思議的看著那張臉。

「怎麼了？伯爵夫人」
「呼、呼呼呼。沒有、失禮了。那個、你和我想像中是完全不同的人呢」
「是嗎。夫人的想像是怎麼樣的、雖然覺得沒有關係⋯⋯」

看著為真面目思考著的女騎士、希莫娜更想笑了。那個伊麗莎的反應、越來越納悶的歪著頭。
然後、看著被逗笑的妻子的萊納斯、不高興的嘟噥。

「你該不會⋯⋯真的是、傳聞中的『巴爾巴斯特的姫騎士』吧？」
「⋯⋯那個叫法不太喜歡」

伊麗莎小小的吹著鼻子。

「對我來說、這個異名把我至今驚心動魄的武勇都給覆蓋了」
「阿啦、先不說武勇、驚心動魄嗎？還真是浪漫啊」
「被浪漫吸引的、只有軟弱的男人哦伯爵夫人。比起那些只會喝著泥水的盜賊、我的心中更希望想有一戰的夙願」
「那太失禮了。⋯⋯我們、應該再早點兒說吧？到現在為止的無聊時間真是有點可惜」
「同感。睡不到另外談、睡起來真是不舒服。能夠稍微磨點時間也可以」

究竟是什麼觸動了心弦，突然打成一片。
咬著牙齒完全不知道女人之間的會話、屬於局外人的萊納斯完全不被理睬。

（那個老頭、根本沒有看女人的眼光⋯⋯）

面對給自己與伊麗莎引線的那個老陰謀家、心中惡狠狠的咒罵著。想起來、就覺得那個滿臉皺紋得意洋洋的表情浮現就在馬車的玻璃、非常可氣。

（還有、這種不知禮儀知騎馬率軍的傢伙、真的是王國騎士的精華嗎？）

這樣想著、窺視了一下與妻子在會話正興的女騎士的側臉。

王國騎士最精鋭中的最精鋭、近衛第二騎士團。在極端的實力主義與過酷的選拔過程中誕生的猛者、史上最年少的女性團長。

那就是她──伊麗莎・蘿茲蒙德・巴爾巴斯特的頭銜。
但是、從自墮落的在護衛對象面前睡覺、現在還和妻子談笑風聲的身影來看、實在是很難相信。難道說、又被拉瓦萊侯爵騙了。

但是、

「⋯⋯不用那麼擔心，伯爵」

伊麗莎也不看這邊，就像是看透了萊納斯。

「咦？」
「什、什麼⋯⋯！？」

困惑的希莫娜還有被看透而慌張的萊納斯。在兩人的眼前、伊麗莎放膽的支起肘用手掌托住臉頰。

「對陰謀老頭的命令不用過度猜測、他是把近衛大方的借出來的人。而且、我也是騎士。這樣說有點不勝惶恐、作為光榮的近衛任命為第二騎士團的這個身體。只要說了守護對方的話、就必定會拼死守護」

必要的話甚至會豁出生命，就是這意思。就像是和希莫娜談家裡閑話一樣、沒有虛張聲勢的意思。
就這樣、沒有任何誇耀之意。她是真心的、為了騎士的任務無論什麼時候都可以死。感覺不到是為了什麼特別的事情、才這樣說的。
知道貴族社會的陰慘裏側、見過好幾次虛虛實實的策略的萊納斯、能夠明確感覺到。

「是⋯⋯嗎。能夠得到如此剛毅的護衛、真是幸運」

──這個女人，沒有謊言。

「說的事情好像有點不一樣、算了。對了、夫人。你在說什麼？」
「咦、嗯嗯。確實⋯⋯」

然後，恢復到了女人們的平凡的會話。
但是，萊納斯看著她的眼神完全變了。

（這傢伙也是⋯⋯鬼子、嗎）

畏怖與不適感出現的同時、聽著伊麗莎・蘿茲蒙德・巴爾巴斯特的話想起來了。

大貴族巴爾巴斯特侯爵家的長女、那女人的劍術比男人還要厲害。斷言拒絶柔弱的婚約者、從家裡出奔的悍馬。到現在為止、都是讓聞者苦笑的女人。但是、她的異常經歴有著這樣的故事。

為了躲避實家的婚約者、拒絶來者的實力主義、只能而進入第二近衛騎士團的門戸。阿爾凱爾在此起用了女性加入騎士團、就是這樣。雖然圍圍有擔心的聲音、但是她的起用試驗還是合格了。⋯⋯在實技試驗中的對戰對手志願者中有兩人被送到了教會、一人用治癒魔法無效死了。好像是在試驗中她作為女兒身帶劍被侮辱、而勃然大怒。面對那個活著的人、她說並沒有下死手、只是對手太弱了、這樣的供述。也就是說、殺死死去的人不是故意的。

那沒關係。雖然有點不好、但那是追求強者的第二騎士團的問題。他的問題、不在這裡。

可怕的是當時的伊麗莎、才十三歳。當時啟用作為騎士的時候、還不滿十五歳的年齢制限。在申請的文件中、也就是說是假名。當時不僅是第二、而是近衛全體都騒然了。違反規則的人合格是無效的。但是、主席合格者覺得捨棄這個人材太過可惜。喧鬧與驚訝的論議結果、便是作為被當時的第二騎士團長承認了。她那時就成為了王國騎士。

被承認為騎士、被實家的侯爵以外賦予了騎士的爵位。也就是說在王國的法律上、已經是實家與別家了。巴爾巴斯特家想把伊麗莎帶回、讓女兒嫁給男人是不可能了。這是聞所未聞的離家出走、然後婚約便破滅了。後來有害怕的人而修改了法典。現在也沒有了模仿的人。

此後、伊麗莎在自己的特例承認的第二騎士團下、建立了大大小小的武功。這便是十年後的現在、史上最年少的王國最強騎士團的頂點⋯⋯⋯

這種才能、實在是過於異端。作為千金不相稱的劍術、變成騎士與家裡斷絶關係的思考回路、把試驗的相手殺死的平靜做法、坦然的說出來。侯爵家這種權門中的權門、卻生出了這種女人。

這就意味著、這個女騎士與托利烏斯與安麗埃塔──或者說、優妮是同類。從傳統與榮光的貴族之子、異形的突然變異。是借人類的肚子出生的、怪物之子。
那種忌諱的弟弟的同類、與那個萊納斯・斯特萊茵・歐布尼爾絶對無法相容的同類。

（吃屎⋯⋯這就是覺得討厭的、道理嗎）

又一次，怎麼想。
這個近衛的樞要只是一介騎士卻把伯爵不當回事的態度。對抹黑自己的血統而不覺得羞恥的精神。這樣考慮起來、那個被人唾棄的托利烏斯・疏日南・歐布尼爾實在是太像這個女人了。只是、對長生相比、對自己的死亡無關痛癢這點不一樣而已。

（好，好。就是以夷制夷的說法吧）

拉瓦萊說、托利烏斯有著能夠使用藥物進行洗腦的可能性。這次、要是從領地沃爾丹進入的話、他有可能會向萊納斯伸出毒牙。所以為此準備的、便是以國境巡檢為名目派遣的、伊麗莎率領的近衛最強的第二騎士團。

萊納斯、已經察覺到了拉瓦萊期待著自己的作用。作為釣托利烏斯的餌。那傢伙很沒義道把自己推向了風口浪尖、然後讓王國最高戰力來擊潰對方。當然、這是顯而易見的陷阱、對方也會猶豫、その場合は向こうが動かぬ內に諸々の無理難題を押し付け行動の自由を奪う。

心情不好。本來在王都的在最高護衛下、現在卻成為有可能被扔掉的棋子。當然會不高興。

但是、或許這次有殺死宿敵的可能性、僅僅如此。就算是把對方找來、也找不到冤枉的方法。而且、這次實行命令的是第二近衛。雖然討厭這種有風險的命令、現在只能靠運氣與自身的器量解決憂患了。

⋯⋯這樣想的話，至少有心情會好點。

───

「哦、景色變了。那是有名的沃爾丹葡萄田吧」
「真是新鮮的東西啊。還是第一次看到」

萊納斯在憂鬱的時候、那個兩個女人卻在悠閑看地著窗外的景色。
高地的山野，在那個平緩的丘陵上，描繪著一條叫做果園的寬廣地毯。赤紫色夫人葡萄果實累累，添加了鮮艷的綠色。
沃爾丹州是有名葡萄酒、赤霞珠的產地。而且在大地與藝術之國稱的、豐收的阿爾凱爾王國、一句『有名的葡萄酒產地』就要讓候補們付出許多的苦勞。

「你是怎麼了？這不是很令人懷念的景象嗎？」

希莫娜、難得的心情很好。這是因為旅行的高漲氣氛、以及與伊麗莎會話的原因吧。
但是、萊納斯可不喜歡這愉快的話題。

「我也是第一次看到⋯畢竟、幾乎都是在王都生活」
「⋯⋯咦？」

這是完全不想的回答嗎、希莫娜眼睛睜得老大。萊納斯好像又有什麼壊主意了。真的是這樣子。

「父親、對做成領地內葡萄酒的阿蒙川的水非常懷戀吧。成為當主後、就一直在布洛森奴。當然、也是在養育我的。離開沃爾丹、今天也是第一次」
「那麼貴公當主就任後到這裡來不就可以了？」

意外的想法、伊麗莎擦嘴了。真是不想向外人說明、她的角色實質上就是拉瓦萊用來監督的。但是聽了就必須回答。

「沒有那樣的空閑。因為前年才剛剛當主交替完畢、王都的政務就和山一樣多。去年的婚禮⋯⋯也是發生了各種各樣的事情」

說了下非常白痴的話。前代是在王都玩物喪志了、所以接下來的當代、才繼承的二年內原地踏步。沃爾丹的土地在這二代都是這樣、除了在紙上都是不知道領地的領主。
聽了那個的伊麗莎，用順便評估一樣的眼睛看著萊納斯。

「呼嗚？這不是很明顯的荒廢嗎。連街道沿邊都不知道」
「⋯⋯最近的近衛，不僅是警衛，連監查都承包了嗎？」
「你」
「不、好了。確實是僭越的發言。是我不好、不好意思伯爵」

然後又愉快的笑了起來。

「貴公明明看起來是會自衛的類型。可那個老頭卻把我借給了你」
「什麼意思？」
「不是什麼大不了的事情。嘛、要守護的對象意外的強呢」
「⋯⋯更加聽不明白了」

萊納斯看向了窗外、來路不明的女騎士也把頭轉向了一邊。
露出了冷笑、就像是這一邊完全是騙人一樣。這個態度、就跟讓人聯想到的那個狂人一樣讓人感到不快。果然這個女人、不想跟她說話。萊納斯這麼想著。

然後、慢慢地、想起來。

現在、對托利烏斯的那股厭惡感正不斷的涌現出來。現在伯爵回到領地上、要把給予子爵的分地收回來是不可能的了。現在就連跟他面對面也非常的不爽。
就連工作時心情也沉重了起來。但是、考慮一下。反過來說在這個地方、那個男人就完全無法違抗自己的命令。並且在有著保護的情況下，也能夠進行監查、失分是也能夠進行叱責以懲罰。
充其量、這只是對自己工作的鼓勵而已。那個男人對自己來說那個不快感就如同蟑螂的惡害一樣。只要進入視野、就想要毀掉。
首先去領地的宅邸、找那在瑪爾蘭閉門不出的傢伙吧。反正只要隨便找個藉口、就能和近衛一天共乗去那裡。瑪爾蘭郡是沃爾丹州的一部分。名目上、這也是巡檢。

在馬車到達目的地的那段時間、萊納斯這樣想著進行。

──但是，事情沒能像那樣進行。

「哎呀，好久不見了哥哥！還有義姉！跑了這麼遠的旅路、辛苦了」

在那個看到州都沃爾丹的一座小山丘上。這之上便建好了屋子前來迎接萊納斯帶著疑似家臣的托利烏斯・疏日南・歐布尼爾樂天地問候著。
下車的時候便聽到了那個聲音、萊納斯在驚訝之餘不禁又跳上了馬車。作為執事的男人一瞬間臉也發青了、

「嘻！？」

短暫的悲鳴。

另一方面希莫娜聽到這話、多少也些吃驚的眨了眨眼睛、然後馬上變成了高興的回話。

「啊啦、托利烏斯卿。出來相迎辛苦了。這邊才是、好久不見」
「義姉也沒有多大改變──不、看著你精神這比什麼都好」

在二人笑嘻嘻的談話間、萊納斯的硬直也解除了。
ギギギ、如同生銹一樣的動作、轉向托利烏斯。

「⋯⋯你這混蛋。為什麼、會在這裡？」

這句話、讓托利烏斯看起來非常意外的聳著肩膀。

「哪裡有為什麼、哥哥今天就要來拜訪了、所以到這裡來歡迎了。有何不可思議？」

一下子、就理虧了。萊納斯打算到達沃爾丹後、用權限把這個男人的行動束縛便是第一目的。考慮過的手段之一、便是以出迎領地運營的協議為名目、把他多次從瑪爾蘭叫來。可氣的是、這個最初的手段被漂亮的躲開了。

但是、以目的來說托利烏斯會到領地來是很難考慮的。去年除了萊納斯與希莫娜的婚禮之外、這個男人完全沒有離開領地的情報、完全不是會移動的性格。像烏亀一樣縮在領地裡才是這個男人的做法、明明還沒有被傳喚到沃爾丹的時間才對。

究竟是什麼目的。
視線暴露的同時、托利烏斯陽光的拍著手。

「那麼、我的家臣也要打下招呼才行了。畢竟初次見面的人也在」

催促著、背後兩為代表出現後跪在他們面前。一邊通過對話知道了、那一邊的萊納斯則是完全沒有印象。

「伯爵閣下以及奧方樣、初次見面。托利烏斯子爵的家臣、維克托爾・德拉克洛瓦・拉瓦萊。以後、還請多多包涵」

金髮碧眼、是比托利烏斯更像高雅的貴公子的男人、漂亮的自稱舉止。拉瓦萊伯爵說過在有一人跟在拉瓦萊身邊。那是為了討好老侯爵而卑屈地把女人送過去的，恐怕就是那傢伙的孩子。也就是說、這傢伙是過繼給拉瓦萊的、就是這個男人。恐怕是作為庶子作為伯爵家令息被這樣使用而奔潰了吧、所以模仿著那個老人⋯⋯萊納斯這樣考慮。

「⋯⋯杜耶・舒華澤、。ふ、能夠再次遇到閣下、真是不勝榮幸」

明顯是因為勉強畏畏縮縮的粗魯男人、是萊納斯見過好幾次托作為裡烏茲部下的武官。他的問候、想必已經練習很多次了、與去年在王都想比、那個聽到了一些禮貌的話。

「雙方、大儀。歐布尼爾伯爵家當主、萊納斯・斯特萊茵。⋯⋯咦、托利烏斯。難道你、就是因為為了見面才到這裡的？」

面對掃興的臉，托利烏斯表情只是看起來快樂的笑咪咪的樣子。
猛烈又討厭的預感，襲向萊納斯。

「這是什麼意思、當然是為了歡迎才來了。撒撒、那是宅邸的方向！跟王都的時候不一樣、為了讓哥哥能夠好好散心所以給整理了一下」
「什！？」

這句話，讓萊納斯的心臟直跳。
托利烏斯直接無視、向背後發話。

「喂、有好好準備吧優妮？」
「是的、主人。打掃和床的整理好了都沒有死角。雖然離晚餐還有點時間、但是要是你們希望的話、可以吃點輕食。洗澡水和煮炊、差不多都完成了」

在那裡的想當然、跪在主人旁邊報告的是那女奴隷。依然如故、作為奴隷的分身份對「奴隷殺手」的奉仕非常執著──不對、比起那個。

（じょ、開玩笑的吧！？）

房間？吃飯？洗澡？那所有的一切，都出手嗎？認識到這個事實的萊納斯、眼前一片黑暗。
托利烏斯搶先到達了沃爾丹、當然萊納斯的宅邸也被動了手腳。如果他的目的是洗腦與暗殺的話、那麼這算是基本完成了。而優妮的話、更加煽動著這個危懼感。至少今天、在這個宅邸的生活全部、都在托利烏斯的宣言下完成。

「開、別開玩笑了！」

脫口而出的怒聲，帶著恐慌的同時臉上也出現了悲鳴之色⋯

「怎麼了，哥哥。有什麼不合適的事情嗎？」
「一切都不合適！作為子爵的身份居然隨意進入伯爵領的宅邸、在沒有許可的情況下對家中進行整理、沒常識也要有個限度！」
「哦，是嗎？」

對於裝傻的托利烏斯、萊納斯巴不得想用雙手掐上去。不、如果可以的話現在就想勒死他。

「是那樣嗎、維克托爾？」
「說起來，也許是那樣的吧。但是、面對遠道而來的哥哥而你進行款待。這樣想不是什麼好事嗎」

主人的背後、對無視了他講悄悄話的他們、萊納斯只能幹瞪著弟弟。

「⋯⋯當然了！你也獲得爵位了、形式上是歐布尼爾伯爵家的別家的人！就算是兄弟、也無權擺弄當家的宅邸！」

這就是這個王國的法律。所以、伊麗莎也是作為騎士與實家拉開了距離、拒絶結婚後斷絶了關係。
但是、希莫娜滿臉不爽的擦嘴了。

「這是故意找茬吧。家財沒有弄壊、也沒有賣掉。是吧、托利烏斯卿？」
「嗯、當然了義姉。侍奉我的孩子們、雖然身分低但是家政都是非常自負的。家具沒有出現任何傷害。晚餐的食材從一粒小麥到一滴油、都是自己負擔的。嘛、確實是借了宅邸裡備蓄的木材、這個希望補償嗎？」
「⋯⋯就這樣。可以了、萊納斯」

聽到希莫娜的話、萊納斯視線完全變冷了。說著什麼小氣巴拉的話、應該是這樣想的。確實、哥哥如此尖酸刻薄的拒絶弟弟的款待、不知事情的人都會怎麼想。

但是在托利烏斯這種特殊情況下要另論。如果拉瓦萊侯爵的話沒錯、這個男人就有把他家的貴族洗腦變成棋子利用的能力。在孝行的假面下、那個宅邸、誰知道被弄了什麼。

理虧的萊納斯、被周圍的騎士看見了。

「所以說。為什麼擔心這邊的客人？這個男人、為了家人都帶著奴隷來了。從遙遠的王都趕來的可是有歷史的近衛第二騎士團、難道要接受愚弟與奴隷們的款待！？」
「是啊⋯⋯」

說話的希莫娜、表情也變得暗淡了。當然。與他人的奴隷解除目光就已經是荒謬絶倫了。現在、還要接觸被奴隷的手碰過的房間、奴隷整理過的床、奴隷作的食物、洗奴隷泡好的洗澡水？別開玩笑了。

托利烏斯雖然給那些野蠻人奴隷打扮成女僕與執事的裝扮。但是、本來傭人作為為貴俯視作為最低限度的身分是必要的。女僕也只能是平民與下級貴族的子女、而從經營來探討執事的話、標準要更加嚴格。就算穿著與禮節沒問題、但奴隷還是奴隷。他們是不能做雜事的人、更加不能作為服侍客人的存在。這就是常識。

希莫娜、就隨便她討厭了、萊納斯這樣想。總之、托利烏斯在王都逗留時、她為他泡茶就已經是無法相信的事情了。不知道的另論、可這個非常識的男人、就是讓奴隷泡茶的。要是出現了這種事情、正經的貴族都會把他趕出去。她已經被這個狂人毒害了。

不管如何、托利烏斯的想法是非常突兀的。

⋯⋯本因如此。

「沒有什麼麻煩的事情、伯爵」

具足發出像是鈴鐺的聲音、是在華麗的馬車下來到達地面的女騎士、伊麗莎・蘿茲蒙德・巴爾巴斯特。

「希望能不要過分地說奴隷的話。我們是不分出生的，也不是就一定高於奴隷的存在。聽了伯爵的話、他們也覺得不舒服」
「巴爾巴斯特卿⋯⋯！」

萊納斯不禁仰望天空。
忘記了。本來的預想中只有托利烏斯的登場、但是這裡還有一人、是與他同樣把常識掛在腦後的人。
哥哥的後面、作為事態原凶的弟弟、對出現的女人恭敬的低頭了。

「這位客人。讓你看到不好的地方、妨礙到下車真是抱歉」
「稍微等了一點也沒有關係。我有耐性這點可是自負的」

十三歳就殺死侮辱自己的對手的女人、真敢這麼說。就跟十歳的小姑娘一樣。

「自報家門有點晚了。伊麗莎・蘿茲蒙德・巴爾巴斯特。只是一個晚輩、作為近衛第二騎士團之身。請隨意」
「這麼說、謝謝⋯⋯瑪爾蘭領主、托利烏斯・疏日南・歐布尼爾。巴爾巴斯特卿的高名早有耳聞」

說完、視線交合。伊麗莎像是在估價一樣、托利烏斯則是表示出笑臉實則毫無興趣。
萊納斯、對二人初次見面的印象就是這種感覺。