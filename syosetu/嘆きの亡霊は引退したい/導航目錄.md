# CONTENTS

嘆きの亡霊は引退したい  
嘆きの亡霊は引退したい　〜最弱ハンターは英雄の夢を見る〜  
嘆息的亡靈想引退　〜最弱獵人做了英雄的夢〜  

作者： 槻影  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E5%98%86%E6%81%AF%E7%9A%84%E4%BA%A1%E9%9D%88%E6%83%B3%E5%BC%95%E9%80%80%E3%80%80%E3%80%9C%E6%9C%80%E5%BC%B1%E7%8D%B5%E4%BA%BA%E5%81%9A%E4%BA%86%E8%8B%B1%E9%9B%84%E7%9A%84%E5%A4%A2%E3%80%9C.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E5%98%86%E6%81%AF%E7%9A%84%E4%BA%A1%E9%9D%88%E6%83%B3%E5%BC%95%E9%80%80%E3%80%80%E3%80%9C%E6%9C%80%E5%BC%B1%E7%8D%B5%E4%BA%BA%E5%81%9A%E4%BA%86%E8%8B%B1%E9%9B%84%E7%9A%84%E5%A4%A2.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu/嘆きの亡霊は引退したい/導航目錄.md "導航目錄")




## [第一章](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0)

- [1.成員招募](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00010_1.%E6%88%90%E5%93%A1%E6%8B%9B%E5%8B%9F.txt)
- [2.成員招募②](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00020_2.%E6%88%90%E5%93%A1%E6%8B%9B%E5%8B%9F%E2%91%A1.txt)
- [3.成員招募③](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00030_3.%E6%88%90%E5%93%A1%E6%8B%9B%E5%8B%9F%E2%91%A2.txt)
- [4.成員招募④](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00040_4.%E6%88%90%E5%93%A1%E6%8B%9B%E5%8B%9F%E2%91%A3.txt)
- [5.帝都](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00050_5.%E5%B8%9D%E9%83%BD.txt)
- [6.懲罰遊戲](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00060_6.%E6%87%B2%E7%BD%B0%E9%81%8A%E6%88%B2.txt)
- [7.預想之外](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00070_7.%E9%A0%90%E6%83%B3%E4%B9%8B%E5%A4%96.txt)
- [8.闇鍋](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00080_8.%E9%97%87%E9%8D%8B.txt)
- [9.獵人](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00090_9.%E7%8D%B5%E4%BA%BA.txt)
- [10.試試本事](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00100_10.%E8%A9%A6%E8%A9%A6%E6%9C%AC%E4%BA%8B.txt)
- [11.白狼之巢](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00110_11.%E7%99%BD%E7%8B%BC%E4%B9%8B%E5%B7%A2.txt)
- [12.有眼無珠](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00120_12.%E6%9C%89%E7%9C%BC%E7%84%A1%E7%8F%A0.txt)
- [13.弟子](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00130_13.%E5%BC%9F%E5%AD%90.txt)
- [14.白狼之巢②](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00140_14.%E7%99%BD%E7%8B%BC%E4%B9%8B%E5%B7%A2%E2%91%A1.txt)
- [15.MASTER的作风](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00150_15.MASTER%E7%9A%84%E4%BD%9C%E9%A3%8E.txt)
- [16.白狼之巢③](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00160_16.%E7%99%BD%E7%8B%BC%E4%B9%8B%E5%B7%A2%E2%91%A2.txt)
- [17.白狼之巣④](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00170_17.%E7%99%BD%E7%8B%BC%E4%B9%8B%E5%B7%A3%E2%91%A3.txt)
- [18.千之试炼](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00180_18.%E5%8D%83%E4%B9%8B%E8%AF%95%E7%82%BC.txt)
- [19.千变万化](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00190_19.%E5%8D%83%E5%8F%98%E4%B8%87%E5%8C%96.txt)
- [20.千变万化②](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00200_20.%E5%8D%83%E5%8F%98%E4%B8%87%E5%8C%96%E2%91%A1.txt)
- [21.千変万化③](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00210_21.%E5%8D%83%E5%A4%89%E4%B8%87%E5%8C%96%E2%91%A2.txt)
- [22.叹息的亡灵](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00220_22.%E5%8F%B9%E6%81%AF%E7%9A%84%E4%BA%A1%E7%81%B5.txt)
- [23.嘆息的亡靈想引退](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00230_23.%E5%98%86%E6%81%AF%E7%9A%84%E4%BA%A1%E9%9D%88%E6%83%B3%E5%BC%95%E9%80%80.txt)
- [一卷問卷附贈短篇_起始的軌跡](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00235_%E4%B8%80%E5%8D%B7%E5%95%8F%E5%8D%B7%E9%99%84%E8%B4%88%E7%9F%AD%E7%AF%87_%E8%B5%B7%E5%A7%8B%E7%9A%84%E8%BB%8C%E8%B7%A1.txt)


## [第二章](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0)

- [24.逃避现实](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00240_24.%E9%80%83%E9%81%BF%E7%8E%B0%E5%AE%9E.txt)
- [25.战后](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00250_25.%E6%88%98%E5%90%8E.txt)
- [26.训练](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00260_26.%E8%AE%AD%E7%BB%83.txt)
- [27.妥协](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00270_27.%E5%A6%A5%E5%8D%8F.txt)
- [28.危机](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00280_28.%E5%8D%B1%E6%9C%BA.txt)
- [29.噩梦](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00290_29.%E5%99%A9%E6%A2%A6.txt)
- [30.另一件事](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00300_30.%E5%8F%A6%E4%B8%80%E4%BB%B6%E4%BA%8B.txt)
- [31.情绪](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00310_31.%E6%83%85%E7%BB%AA.txt)
- [32.集合](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00320_32.%E9%9B%86%E5%90%88.txt)
- [33.争论](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00330_33.%E4%BA%89%E8%AE%BA.txt)
- [34.无能](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00340_34.%E6%97%A0%E8%83%BD.txt)
- [35.撤退](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00350_35.%E6%92%A4%E9%80%80.txt)
- [36.判断](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00360_36.%E5%88%A4%E6%96%AD.txt)
- [37.对策](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00370_37.%E5%AF%B9%E7%AD%96.txt)
- [38.作战开始](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00380_38.%E4%BD%9C%E6%88%98%E5%BC%80%E5%A7%8B.txt)
- [39.增援](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00390_39.%E5%A2%9E%E6%8F%B4.txt)
- [40.史莱姆](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00400_40.%E5%8F%B2%E8%8E%B1%E5%A7%86.txt)
- [41.史莱姆②](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00410_41.%E5%8F%B2%E8%8E%B1%E5%A7%86%E2%91%A1.txt)
- [42.史莱姆③](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00420_42.%E5%8F%B2%E8%8E%B1%E5%A7%86%E2%91%A2.txt)
- [43.追踪者](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00430_43.%E8%BF%BD%E8%B8%AA%E8%80%85.txt)
- [44.灾祸之源](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00440_44.%E7%81%BE%E7%A5%B8%E4%B9%8B%E6%BA%90.txt)
- [45.灾祸之源②](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00450_45.%E7%81%BE%E7%A5%B8%E4%B9%8B%E6%BA%90%E2%91%A1.txt)
- [46.兔子](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00460_46.%E5%85%94%E5%AD%90.txt)
- [47.八级](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00470_47.%E5%85%AB%E7%BA%A7.txt)
- [48.八级②](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00480_48.%E5%85%AB%E7%BA%A7%E2%91%A1.txt)
- [49.慈悲](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00490_49.%E6%85%88%E6%82%B2.txt)
- [50.老实人](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00500_50.%E8%80%81%E5%AE%9E%E4%BA%BA.txt)
- [二卷問卷附贈短篇_憧憬と記憶](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00505_%E4%BA%8C%E5%8D%B7%E5%95%8F%E5%8D%B7%E9%99%84%E8%B4%88%E7%9F%AD%E7%AF%87_%E6%86%A7%E6%86%AC%E3%81%A8%E8%A8%98%E6%86%B6.txt)
- [51.冰淇淋](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00510_51.%E5%86%B0%E6%B7%87%E6%B7%8B.txt)
- [52.丧心病狂](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00520_52.%E4%B8%A7%E5%BF%83%E7%97%85%E7%8B%82.txt)
- [53.丧心病狂②](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00530_53.%E4%B8%A7%E5%BF%83%E7%97%85%E7%8B%82%E2%91%A1.txt)
- [54.丧心病狂③](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00540_54.%E4%B8%A7%E5%BF%83%E7%97%85%E7%8B%82%E2%91%A2.txt)
- [55.叹息之亡灵想要引退②](00010_%E7%AC%AC%E4%BA%8C%E7%AB%A0/00550_55.%E5%8F%B9%E6%81%AF%E4%B9%8B%E4%BA%A1%E7%81%B5%E6%83%B3%E8%A6%81%E5%BC%95%E9%80%80%E2%91%A1.txt)


## [第三章](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0)

- [56.坏毛病](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00560_56.%E5%9D%8F%E6%AF%9B%E7%97%85.txt)
- [57.不对劲](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00570_57.%E4%B8%8D%E5%AF%B9%E5%8A%B2.txt)
- [58.为时已晚](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00580_58.%E4%B8%BA%E6%97%B6%E5%B7%B2%E6%99%9A.txt)
- [59.秘籍](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00590_59.%E7%A7%98%E7%B1%8D.txt)
- [60.差异](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00600_60.%E5%B7%AE%E5%BC%82.txt)
- [61.正好抵达](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00610_61.%E6%AD%A3%E5%A5%BD%E6%8A%B5%E8%BE%BE.txt)
- [62.酒会①](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00620_62.%E9%85%92%E4%BC%9A%E2%91%A0.txt)
- [63.酒会②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00630_63.%E9%85%92%E4%BC%9A%E2%91%A1.txt)
- [64.酒会③](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00640_64.%E9%85%92%E4%BC%9A%E2%91%A2.txt)
- [65.照烧](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00650_65.%E7%85%A7%E7%83%A7.txt)
- [66.负债](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00660_66.%E8%B4%9F%E5%80%BA.txt)
- [67.强敌](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00670_67.%E5%BC%BA%E6%95%8C.txt)
- [68.差距](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00680_68.%E5%B7%AE%E8%B7%9D.txt)
- [69.宝具](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00690_69.%E5%AE%9D%E5%85%B7.txt)
- [70.宝具②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00700_70.%E5%AE%9D%E5%85%B7%E2%91%A1.txt)
- [71.筹钱](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00710_71.%E7%AD%B9%E9%92%B1.txt)
- [72.筹钱②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00720_72.%E7%AD%B9%E9%92%B1%E2%91%A1.txt)
- [73.筹钱③](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00730_73.%E7%AD%B9%E9%92%B1%E2%91%A2.txt)
- [74.交涉](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00740_74.%E4%BA%A4%E6%B6%89.txt)
- [75.交涉②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00750_75.%E4%BA%A4%E6%B6%89%E2%91%A1.txt)
- [76.蔓延](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00760_76.%E8%94%93%E5%BB%B6.txt)
- [77.蔓延②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00770_77.%E8%94%93%E5%BB%B6%E2%91%A1.txt)
- [78.鉴定结果](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00780_78.%E9%89%B4%E5%AE%9A%E7%BB%93%E6%9E%9C.txt)
- [79.理想与现实](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00790_79.%E7%90%86%E6%83%B3%E4%B8%8E%E7%8E%B0%E5%AE%9E.txt)
- [80.竞拍](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00800_80.%E7%AB%9E%E6%8B%8D.txt)
- [81.竞拍②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00810_81.%E7%AB%9E%E6%8B%8D%E2%91%A1.txt)
- [82.竞拍③](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00820_82.%E7%AB%9E%E6%8B%8D%E2%91%A2.txt)
- [83.竞拍④](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00830_83.%E7%AB%9E%E6%8B%8D%E2%91%A3.txt)
- [84.竞拍⑤](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00840_84.%E7%AB%9E%E6%8B%8D%E2%91%A4.txt)
- [85.废人](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00850_85.%E5%BA%9F%E4%BA%BA.txt)
- [86.有趣的男人](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00860_86.%E6%9C%89%E8%B6%A3%E7%9A%84%E7%94%B7%E4%BA%BA.txt)
- [87.危险的宝具](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00870_87.%E5%8D%B1%E9%99%A9%E7%9A%84%E5%AE%9D%E5%85%B7.txt)
- [88.危险的宝具②](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00880_88.%E5%8D%B1%E9%99%A9%E7%9A%84%E5%AE%9D%E5%85%B7%E2%91%A1.txt)
- [89.恶](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00890_89.%E6%81%B6.txt)
- [90.拼图](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00900_90.%E6%8B%BC%E5%9B%BE.txt)
- [91.叹息的亡灵想引退③](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00910_91.%E5%8F%B9%E6%81%AF%E7%9A%84%E4%BA%A1%E7%81%B5%E6%83%B3%E5%BC%95%E9%80%80%E2%91%A2.txt)
- [92.面具](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00920_92.%E9%9D%A2%E5%85%B7.txt)
- [93.功劳](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00930_93.%E5%8A%9F%E5%8A%B3.txt)
- [94.蛋糕](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00940_94.%E8%9B%8B%E7%B3%95.txt)
- [95.援手](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00950_95.%E6%8F%B4%E6%89%8B.txt)
- [96.谢礼](00020_%E7%AC%AC%E4%B8%89%E7%AB%A0/00960_96.%E8%B0%A2%E7%A4%BC.txt)


## [第四章](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0)

- [97.秘密](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00970_97.%E7%A7%98%E5%AF%86.txt)
- [98.工作](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00980_98.%E5%B7%A5%E4%BD%9C.txt)
- [99.连带责任](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/00990_99.%E8%BF%9E%E5%B8%A6%E8%B4%A3%E4%BB%BB.txt)
- [100.逃避现实②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01000_100.%E9%80%83%E9%81%BF%E7%8E%B0%E5%AE%9E%E2%91%A1.txt)
- [101.发挥本领](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01010_101.%E5%8F%91%E6%8C%A5%E6%9C%AC%E9%A2%86.txt)
- [102.发挥本领②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01020_102.%E5%8F%91%E6%8C%A5%E6%9C%AC%E9%A2%86%E2%91%A1.txt)
- [103.度假的开始](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01030_103.%E5%BA%A6%E5%81%87%E7%9A%84%E5%BC%80%E5%A7%8B.txt)
- [104.度假的开始②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01040_104.%E5%BA%A6%E5%81%87%E7%9A%84%E5%BC%80%E5%A7%8B%E2%91%A1.txt)
- [105.度假的开始③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01050_105.%E5%BA%A6%E5%81%87%E7%9A%84%E5%BC%80%E5%A7%8B%E2%91%A2.txt)
- [106.愉快的度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01060_106.%E6%84%89%E5%BF%AB%E7%9A%84%E5%BA%A6%E5%81%87.txt)
- [107.愉快的度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01070_107.%E6%84%89%E5%BF%AB%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [108.愉快的度假③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01080_108.%E6%84%89%E5%BF%AB%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A2.txt)
- [109.愉快的度假④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01090_109.%E6%84%89%E5%BF%AB%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A3.txt)
- [110.某度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01100_110.%E6%9F%90%E5%BA%A6%E5%81%87.txt)
- [111.激动的度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01110_111.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87.txt)
- [112.激动的度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01120_112.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [113.某度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01130_113.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [114.激动的度假③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01140_114.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A2.txt)
- [115.激动的度假④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01150_115.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A3.txt)
- [116.某度假③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01160_116.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A2.txt)
- [117.激动的度假⑤](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01170_117.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A4.txt)
- [118.激动的度假⑥](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01180_118.%E6%BF%80%E5%8A%A8%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A5.txt)
- [119.刺激的度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01190_119.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87.txt)
- [120.某度假④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01200_120.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A3.txt)
- [121.刺激的度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01210_121.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [122.刺激的度假③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01220_122.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A2.txt)
- [123.刺激的度假④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01230_123.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A3.txt)
- [124.某度假⑤](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01240_124.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A4.txt)
- [125.某度假⑥](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01250_125.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A5.txt)
- [126.刺激的度假⑤](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01260_126.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A4.txt)
- [127.刺激的度假⑥](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/01270_127.%E5%88%BA%E6%BF%80%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A5.txt)
- [128.某度假⑦](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001280_128.%E6%9F%90%E5%BA%A6%E5%81%87%E2%91%A6.txt)
- [129.习以为常](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001290_129.%E4%B9%A0%E4%BB%A5%E4%B8%BA%E5%B8%B8.txt)
- [130.自豪](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001300_130.%E8%87%AA%E8%B1%AA.txt)
- [131.自豪②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001310_131.%E8%87%AA%E8%B1%AA%E2%91%A1.txt)
- [132.自豪③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001320_132.%E8%87%AA%E8%B1%AA%E2%91%A2.txt)
- [133.开心的度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001330_133.%E5%BC%80%E5%BF%83%E7%9A%84%E5%BA%A6%E5%81%87.txt)
- [134.开心的度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001340_134.%E5%BC%80%E5%BF%83%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [135.开心的度假③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001350_135.%E5%BC%80%E5%BF%83%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A2.txt)
- [136.开心的度假④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001360_136.%E5%BC%80%E5%BF%83%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A3.txt)
- [137.糟糕的度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001370_137.%E7%B3%9F%E7%B3%95%E7%9A%84%E5%BA%A6%E5%81%87.txt)
- [138.糟糕的度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001380_138.%E7%B3%9F%E7%B3%95%E7%9A%84%E5%BA%A6%E5%81%87%E2%91%A1.txt)
- [139.既视感](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001390_139.%E6%97%A2%E8%A7%86%E6%84%9F.txt)
- [140.水煮龙](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001400_140.%E6%B0%B4%E7%85%AE%E9%BE%99.txt)
- [141.小失误](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001410_141.%E5%B0%8F%E5%A4%B1%E8%AF%AF.txt)
- [141.5.恶梦](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001420_141.5.%E6%81%B6%E6%A2%A6.txt)
- [142.跟踪狂](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001430_142.%E8%B7%9F%E8%B8%AA%E7%8B%82.txt)
- [143.和解](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001440_143.%E5%92%8C%E8%A7%A3.txt)
- [144.和解②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001450_144.%E5%92%8C%E8%A7%A3%E2%91%A1.txt)
- [145.和解③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001460_145.%E5%92%8C%E8%A7%A3%E2%91%A2.txt)
- [146.度假的结束](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001470_146.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F.txt)
- [147.度假的结束②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001480_147.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F%E2%91%A1.txt)
- [148.度假的结束③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001490_148.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F%E2%91%A2.txt)
- [149.度假的结束④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001500_149.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F%E2%91%A3.txt)
- [150.度假的结束⑤](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001510_150.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F%E2%91%A4.txt)
- [151.度假的结束⑥](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001520_151.%E5%BA%A6%E5%81%87%E7%9A%84%E7%BB%93%E6%9D%9F%E2%91%A5.txt)
- [152.结束与开始](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001530_152.%E7%BB%93%E6%9D%9F%E4%B8%8E%E5%BC%80%E5%A7%8B.txt)
- [153.结束与开始②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001540_153.%E7%BB%93%E6%9D%9F%E4%B8%8E%E5%BC%80%E5%A7%8B%E2%91%A1.txt)
- [154.结束与开始③](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001550_154.%E7%BB%93%E6%9D%9F%E4%B8%8E%E5%BC%80%E5%A7%8B%E2%91%A2.txt)
- [155.叹息的亡灵想隐退④](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001560_155.%E5%8F%B9%E6%81%AF%E7%9A%84%E4%BA%A1%E7%81%B5%E6%83%B3%E9%9A%90%E9%80%80%E2%91%A3.txt)
- [155.5.恶梦②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001570_155.5.%E6%81%B6%E6%A2%A6%E2%91%A1.txt)
- [156.魔女](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001580_156.%E9%AD%94%E5%A5%B3.txt)
- [157.温泉挖掘](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001590_157.%E6%B8%A9%E6%B3%89%E6%8C%96%E6%8E%98.txt)
- [158.度假](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001600_158.%E5%BA%A6%E5%81%87.txt)
- [159.度假②](00030_%E7%AC%AC%E5%9B%9B%E7%AB%A0/001610_159.%E5%BA%A6%E5%81%87%E2%91%A1.txt)


## [第五章](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0)

- [160.归来(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001620_160.%E5%BD%92%E6%9D%A5(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [161.白剑的集会(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001630_161.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [162.白剑的集会②(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001640_162.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A%E2%91%A1(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [163.白剑的集会③(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001650_163.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A%E2%91%A2(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [164.白剑的集会④(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001660_164.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A%E2%91%A3(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [165.白剑的集会⑤(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001670_165.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A%E2%91%A4(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [166.白剑的集会⑥(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001680_166.%E7%99%BD%E5%89%91%E7%9A%84%E9%9B%86%E4%BC%9A%E2%91%A5(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [167.叹灵会议①(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001690_167.%E5%8F%B9%E7%81%B5%E4%BC%9A%E8%AE%AE%E2%91%A0(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [168.叹灵会议②(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001700_168.%E5%8F%B9%E7%81%B5%E4%BC%9A%E8%AE%AE%E2%91%A1(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [169.误算(粗校版)](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001710_169.%E8%AF%AF%E7%AE%97(%E7%B2%97%E6%A0%A1%E7%89%88).txt)
- [170.绒毯](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001720_170.%E7%BB%92%E6%AF%AF.txt)
- [171.责任与威光](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001730_171.%E8%B4%A3%E4%BB%BB%E4%B8%8E%E5%A8%81%E5%85%89.txt)
- [172.挑选](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001740_172.%E6%8C%91%E9%80%89.txt)
- [173.挑选②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001750_173.%E6%8C%91%E9%80%89%E2%91%A1.txt)
- [174.启程](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001770_174.%E5%90%AF%E7%A8%8B.txt)
- [175.启程②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001780_175.%E5%90%AF%E7%A8%8B%E2%91%A1.txt)
- [176.违和感](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001790_176.%E8%BF%9D%E5%92%8C%E6%84%9F.txt)
- [177.洞悉](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001800_177.%E6%B4%9E%E6%82%89.txt)
- [178.洞悉②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001810_178.%E6%B4%9E%E6%82%89%E2%91%A1.txt)
- [179.护卫](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001820_179.%E6%8A%A4%E5%8D%AB.txt)
- [180.护卫②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001830_180.%E6%8A%A4%E5%8D%AB%E2%91%A1.txt)
- [181.护卫③](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001840_181.%E6%8A%A4%E5%8D%AB%E2%91%A2.txt)
- [182.信赖](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001850_182.%E4%BF%A1%E8%B5%96.txt)
- [183.信赖②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001860_183.%E4%BF%A1%E8%B5%96%E2%91%A1.txt)
- [184.信赖③](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001870_184.%E4%BF%A1%E8%B5%96%E2%91%A2.txt)
- [185.床单幽灵](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001880_185.%E5%BA%8A%E5%8D%95%E5%B9%BD%E7%81%B5.txt)
- [186.才能开花](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001890_186.%E6%89%8D%E8%83%BD%E5%BC%80%E8%8A%B1.txt)
- [187.乱数调整](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001900_187.%E4%B9%B1%E6%95%B0%E8%B0%83%E6%95%B4.txt)
- [188.幽灵部队](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001910_188.%E5%B9%BD%E7%81%B5%E9%83%A8%E9%98%9F.txt)
- [189.幽灵部队②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001920_189.%E5%B9%BD%E7%81%B5%E9%83%A8%E9%98%9F%E2%91%A1.txt)
- [190.万全的准备](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001930_190.%E4%B8%87%E5%85%A8%E7%9A%84%E5%87%86%E5%A4%87.txt)
- [191.出港](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001940_191.%E5%87%BA%E6%B8%AF.txt)
- [192.空旅](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001950_192.%E7%A9%BA%E6%97%85.txt)
- [192.5.凯恰恰卡的忧郁](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001960_192.5.%E5%87%AF%E6%81%B0%E6%81%B0%E5%8D%A1%E7%9A%84%E5%BF%A7%E9%83%81.txt)
- [193.幸运](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001970_193.%E5%B9%B8%E8%BF%90.txt)
- [194.最棒的伙伴](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001980_194.%E6%9C%80%E6%A3%92%E7%9A%84%E4%BC%99%E4%BC%B4.txt)
- [195.最棒的伙伴②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/001990_195.%E6%9C%80%E6%A3%92%E7%9A%84%E4%BC%99%E4%BC%B4%E2%91%A1.txt)
- [196.最棒的伙伴③](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002000_196.%E6%9C%80%E6%A3%92%E7%9A%84%E4%BC%99%E4%BC%B4%E2%91%A2.txt)
- [197.活着的灾厄](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002010_197.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84.txt)
- [198.活着的灾厄②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002020_198.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84%E2%91%A1.txt)
- [199.活着的灾厄③](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002030_199.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84%E2%91%A2.txt)
- [200.活着的灾厄④](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002040_200.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84%E2%91%A3.txt)
- [201.活着的灾厄⑤](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002050_201.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84%E2%91%A4.txt)
- [203.可靠的男人](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002060_203.%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA.txt)
- [204.叹息的亡灵想要引退⑤](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002070_204.%E5%8F%B9%E6%81%AF%E7%9A%84%E4%BA%A1%E7%81%B5%E6%83%B3%E8%A6%81%E5%BC%95%E9%80%80%E2%91%A4.txt)
- [205.不可靠的男人](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002080_205.%E4%B8%8D%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA.txt)
- [206.可靠的妖怪](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002090_206.%E5%8F%AF%E9%9D%A0%E7%9A%84%E5%A6%96%E6%80%AA.txt)
- [207.乡下人](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002100_207.%E4%B9%A1%E4%B8%8B%E4%BA%BA.txt)
- [208.活着的灾厄SP](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002110_208.%E6%B4%BB%E7%9D%80%E7%9A%84%E7%81%BE%E5%8E%84SP.txt)
- [209.可靠的男人②](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002120_209.%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A1.txt)
- [210.可靠的男人③](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002130_210.%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A2.txt)
- [211.可靠的男人④](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/002140_211.%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A3.txt)


## [第六章](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0)

- [212.评价](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002150_212.%E8%AF%84%E4%BB%B7.txt)
- [213.评价②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002160_213.%E8%AF%84%E4%BB%B7%E2%91%A1.txt)
- [214.局外人](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002170_214.%E5%B1%80%E5%A4%96%E4%BA%BA.txt)
- [215.局外人②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002180_215.%E5%B1%80%E5%A4%96%E4%BA%BA%E2%91%A1.txt)
- [216.局外人③](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002190_216.%E5%B1%80%E5%A4%96%E4%BA%BA%E2%91%A2.txt)
- [217.可靠的男人⑤](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002200_217.%E5%8F%AF%E9%9D%A0%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A4.txt)
- [218.一直逃避的男人](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002210_218.%E4%B8%80%E7%9B%B4%E9%80%83%E9%81%BF%E7%9A%84%E7%94%B7%E4%BA%BA.txt)
- [219.一直逃避的男人②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002220_219.%E4%B8%80%E7%9B%B4%E9%80%83%E9%81%BF%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A1.txt)
- [220.一直逃避的男人③](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002230_220.%E4%B8%80%E7%9B%B4%E9%80%83%E9%81%BF%E7%9A%84%E7%94%B7%E4%BA%BA%E2%91%A2.txt)
- [221.爱好会](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002240_221.%E7%88%B1%E5%A5%BD%E4%BC%9A.txt)
- [222.愛好会②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002250_222.%E6%84%9B%E5%A5%BD%E4%BC%9A%E2%91%A1.txt)
- [223.愛好会③](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002260_223.%E6%84%9B%E5%A5%BD%E4%BC%9A%E2%91%A2.txt)
- [224.糟糕的组织](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002270_224.%E7%B3%9F%E7%B3%95%E7%9A%84%E7%BB%84%E7%BB%87.txt)
- [225.学习](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002280_225.%E5%AD%A6%E4%B9%A0.txt)
- [226.灯火骑士团](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002290_226.%E7%81%AF%E7%81%AB%E9%AA%91%E5%A3%AB%E5%9B%A2.txt)
- [227.糟糕的组织②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002300_227.%E7%B3%9F%E7%B3%95%E7%9A%84%E7%BB%84%E7%BB%87%E2%91%A1.txt)
- [228.糟糕的组织③](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002310_228.%E7%B3%9F%E7%B3%95%E7%9A%84%E7%BB%84%E7%BB%87%E2%91%A2.txt)
- [229.“那个”](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002320_229.%E2%80%9C%E9%82%A3%E4%B8%AA%E2%80%9D.txt)
- [230.叹息的恶灵](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002330_230.%E5%8F%B9%E6%81%AF%E7%9A%84%E6%81%B6%E7%81%B5.txt)
- [231.叹息的恶灵②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002340_231.%E5%8F%B9%E6%81%AF%E7%9A%84%E6%81%B6%E7%81%B5%E2%91%A1.txt)
- [232.叹息的恶灵③](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002350_232.%E5%8F%B9%E6%81%AF%E7%9A%84%E6%81%B6%E7%81%B5%E2%91%A2.txt)
- [233.真正的boss](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002360_233.%E7%9C%9F%E6%AD%A3%E7%9A%84boss.txt)
- [234.“那个”②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002370_234.%E2%80%9C%E9%82%A3%E4%B8%AA%E2%80%9D%E2%91%A1.txt)
- [235.解决方案](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002380_235.%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88.txt)
- [236.神谕](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002390_236.%E7%A5%9E%E8%B0%95.txt)
- [237.叹息的恶灵④](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002400_237.%E5%8F%B9%E6%81%AF%E7%9A%84%E6%81%B6%E7%81%B5%E2%91%A3.txt)
- [238.大地之键](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002410_238.%E5%A4%A7%E5%9C%B0%E4%B9%8B%E9%94%AE.txt)
- [239.白狐](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002420_239.%E7%99%BD%E7%8B%90.txt)
- [240.白狐②](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002430_240.%E7%99%BD%E7%8B%90%E2%91%A1.txt)
- [241.影武者](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002440_241.%E5%BD%B1%E6%AD%A6%E8%80%85.txt)
- [242.帮助](00050_%E7%AC%AC%E5%85%AD%E7%AB%A0/002450_242.%E5%B8%AE%E5%8A%A9.txt)

