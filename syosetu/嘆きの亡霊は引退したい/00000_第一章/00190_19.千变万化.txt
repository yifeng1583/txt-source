似乎總算是平安停下來了。

我放下護住頭部的胳膊，正要站起來的時候注意到了。

無傷。雖然受到了很厲害的衝擊，但一點都不痛。好歹是活下來了。

時隔好久才回到地面，可以不用晃來晃去的了。我拚命忍住想吐的衝動。
甩了甩宕機的腦袋，保持清醒。有著很長獵人空白期的我在寶物殿中喪失意識的話基本等同死亡。

嗙嗙地撣去肩膀上的灰塵，總之先來個深呼吸。
心臟仍舊在悲鳴著，不快點平復下來就像是要爆炸似的。
臉也哆嗦著僵硬著。但是，只是看到走馬燈那種程度就搞定也算是了不得的成果吧。

果然『夜天之暗翼』是個徹徹底底的缺陷品。當初設計出這玩意兒的傢伙肯定，是跟我的青梅竹馬們同等程度的瘋子。
減速功能明明是應該最先考慮的。

被我當成緩衝墊的幻影，頭被埋進了牆壁中。

當時沒怎麼看清楚，似乎一共有兩體的樣子。像是被堆起來似的倒下的幻影們身體痙攣著一動也不動。

看起來就算是Level 3寶物殿中的幻影，從背後吃下一發人型導彈也不是對手。厚厚的黑色鎧甲大幅凹了下去，甚至都裂了。

牆邊滾落著巨大的弓和劍，應該是這兩體幻影的吧。

總覺得這幻影跟之前聽到的在大小、形態和顏色上都不太一樣，到底怎麼回事啊。

這個寶物殿中出現的幻影應該是狼才對，但倒下的幻影裝備著似乎只有上級騎士才有的厚重鎧甲，在不好的方向上偏離了預想。

當年、我被強拉進level 3寶物殿的時候應該是稍微……不，應該是相當弱的幻影才對……雖然最近沒怎麼來過寶物殿，但之前是這樣的東西嗎？

也有是徒有其表的可能性。總覺得要吐了。

隨後我緩緩望向周邊。由於剛剛猛站起來而眼前發黑的視野鮮明起來。

之前飛在空中的時候沒有餘力確認，現在所在之處並不是通路而是廣闊的空間。
令人想不到是地下的高高的天花板，不像是狼挖掘出的平坦的地面和牆壁。室內開有窗戶因此很明亮，而且也不潮濕。要是沒有幻影在的話簡直是最棒的房間。

在牆邊發現了熟悉的身影。

變得雜亂的黑髮、白潤的臉頰。看上去雖然沒受傷，但比在氏族之家中碰面的時候憔悴很多的少女。

哎呀，這不是緹諾嘛。由於我的失誤被委以異常委託的緹諾。

旁邊吉爾貝特少年和古萊古先生也在，但都奄奄一息地、同時啞然地看著我。

「ma……master！？」

「發現緹諾啦」

Lucky。

……不不，才不是什麼發現啦吧。

雖然因過於混亂而發出了輕鬆的聲音，在這裡應該好好謝罪才是。

儘管人沒什麼事，但緹諾臉色蒼白，與往常的神態相當不同，充滿了疲勞。
顯然這個level 3的寶物殿帶給了緹諾很大的負擔。

終於到了向後輩展示土下座技能的時刻了嗎。

只能笑了。

對著默默露出笑容的我，吉爾貝特一副拚命的樣子大叫道。

「喔、喂，大叔。後面，後面！」

「誒？」

才不是大叔。是哥哥才對。

最初浮現出的竟然是這樣的想法，是因為我已經由於空白期完全變成了和平呆子了吧。
在寶物殿中大意什麼的，可以說是獵人中的渣渣了。

悠然地回過頭去後出現在視野中的、是與之前被當做緩衝墊同型的幻影。

裝備著黑鐵色鎧甲的巨大幻影。兩體。

剛剛突入進來的時候沒注意到，幻影的頭部並非是人臉而是狼面。而且，右半部分還覆蓋著人類頭骨的假面。

在黑暗中閃耀著的血一般的赤色眼瞳，俯視著不讀氣氛而亂入的我。

肩膀起伏著，粗暴地喘息著。從宛如裂開般的上顎粘稠的唾液滴落下來。

若是之前的我會嚇到腰軟、只是被那目光看著就會吐出來了吧。但是，變成了和平呆子而感覺麻痺的我抱持著別的感想。

嘿— 最近的level 3幻影個頭這麼大啊。進化了啊。
Level 3都這樣了，那level 8得變成啥樣啊。沒再去寶物殿真是太好了。

往昔的我，真是明智。難不成是神嗎？

拿著快碰到房頂的巨大鐵棒的狼騎士，看到滿臉帶笑的我之後退後了一步。後面持有粗大槍械的狼騎士也發出小小的呻吟聲退後了。

鼻子大大地抽動著。銳利的眼睛眯細、慎重地觀察著我。

至此我才終於把握到了現狀，失去了笑容。

啊咧。這樣子，難道說是要死了？危機重重？

似乎它們由於某種理由不能主動攻擊，但我也沒法戰勝能讓緹諾這樣有前途的獵人狼狽不堪的對手。

在拚命思考對策的我身後，古萊古先生戰慄地叫道。

「不可能……那樣的……boss……竟然、膽怯了！？」

……誒？

「膽怯了？」

那才是不可能吧。如果說這些傢伙是狼的話那我就是羊。
還是沒有經過mana·material強化的，只有認定等級高的羊。

然而，在抬頭向上看著的我面前，狼騎士們又後退了一步。

鼻子不停地上下抽動，意識完全排除掉了緹諾她們，只集中在我身上
確實在它們的眼神中能看到強烈的警戒。

到底是在害怕我什麼呢？古萊古先生他們才更恐怖啊。

沿著那視線。注意到了它們目光所集中的地方。
真紅色視線的終點並不是我，而是我的胸前——掛在脖子上的、存放著希特莉史萊姆的金屬膠囊。

我向前踏出一步。狼騎士向後退一步。雖然視線是朝向我的，但並非是在看我。

嗯嗯—？啊咧？

如此巨大的『幻影』只是看到就膽怯了，這個膠囊中到底是放了什麼啊？
我這是帶著什麼玩意兒？

我再向前逼近一步，狼騎士們一齊向後退了兩步。這是完全把我當成有毒的羊來認知的吧。

真是走運。看起來這裡並非是我的葬身之所呢。

我依舊將視線停留在狼騎士身上，向身後喊道。

「緹諾，能跑嗎？」

「是、是的……當然的說！」

得到了元氣滿滿的回答。

這個房間中有三條道路。正面的路被狼騎士堵住了。
不管再怎麼膽怯，也無法否認它們會得出『就算有毒也無所謂』的結論。想要突破巨大的兩體狼騎士是不行的。

「那邊吧」

我指向最近的右側道路。這個寶物殿並不是特別大。一旦緹諾她們休息恢復好大家應該就能一齊脫出了吧。

「那、那個，master。直接打倒它們，是不是更好？」

嗯嗯，就是說呢。能打倒的話那樣比較好呢。

不管三七二十一把希特莉史萊姆扔出去、賭一發對手會死也是一個方案，但把命運托付給完全不清楚具體是什麼情況的史萊姆，風險太高了。

如果維持在膠囊這種狀態能發揮效果的話，那就應該如此使用吧。

我嘆了口氣，像是教導般對可愛的後輩說道。

「緹諾，不能漏看重要的事情哦」

「！！那、是——」

最重要的事情。不言而喻。

那是——自己的性命啊。

堵上性命去戰鬥什麼的，在我看來簡直荒唐。
雖說自己的性命自己負責，所以想賭上性命的話也沒什麼關係，但我絕對不想那麼做。

正在這時，不知從哪兒傳來咯當一聲。隨後緹諾發出了小小的聲音。

「啊—」

視野被黑影填滿。漆黑之鎧向眼前迫近。

受到人型導彈攻擊而倒下的狼騎士復活了，然後逼近到只剩一步的距離。
注意到這點的時候，長度有我身高那麼長的刀刃正從頭上揮落下來。

混入了憤怒和威壓的咆哮與獸臭填滿了我的意識。我像是抽筋一樣身體動不了了。

無法做出反應。連一根手指都動不了。

如斷頭台一般降下的刀刃。

能將人輕鬆一刀兩斷的一擊襲向了我、

——然後，未造成絲毫傷害被彈開了。

「……哈？」

古萊古漏出了聲音。迫近過來的狼騎士瞪大了眼睛。應該是完全在預想外吧。
狼騎士退後了數步，儘管只是一瞬間，也忘記了怨恨而低頭看了看握在手中的雙手劍。

隨後，伴隨著砲擊般的聲音巨大的箭矢命中了我的頭部，但也如同之前那樣被彈開了。

看起來被我衝進來時當成緩衝墊的幻影都沒死呢。

然後，被激怒了。這也是當然的吧。像那樣突然被從後面懟進牆裡的話，換我也會被激怒的。

持有弓的個體、大劍的個體，以及其他兩體狼騎士，怒視著我。

我只能苦笑了。除此之外也做不了什麼。

這就是——死。要死了。

至此，我才想到其實還可以反擊啊。

伸出食指，像槍一樣指向狼騎士們。

左手小指上戴著的寶具——我啟動了彈指之一的『驅動衝擊之戒指』。

指尖上顯現出青色的光，生成了魔法彈丸。

釋放彈丸的前一瞬，我突然想到炫酷的台詞，反射性地說了出來。

「真是遺憾呢。我可是——有十七條命的喲。」


§


寶藏獵人的世界是才能的世界。

以人類本來的身體，是無法訪問充滿著mana·material的寶物殿，並且同魔物和幻影進行戰鬥的。
這也是即使處於當今這個寶藏獵人大放異彩的時代，獵人數卻無法超過一定數量的原因。

對我來說不幸的是，注意到這些是在成為獵人之後這點。

然後，幸運的是在青梅竹馬們當中，沒有才能的人只有我一人這點。

被賦予『嘆息的亡靈』這一名稱的我們隊，除我之外的成員都持有著能輕鬆攻略寶物殿的才能。

然後因此，他們從寶物殿帶回的財富和積累的名譽也帶給了我一點點『福利』。

所以，雖然我沒有才能、勇氣和幹勁，也沒有夢想、希望和運氣，但仍然還活著。

結界指是與彈指同樣有名的戒指型寶具。

效果是、在受到攻擊的時候自動展開一定強度的結界。

直截了當地說……就是只限一次防住攻擊的寶具。

簡單來講，結界指的價值和稀少性雖然會因結界強度和持續時間而不同，但對無論如何都不想死的我來說並不關心持續時間或強度，管它什麼價格有多少買多少。

現在我手裡的數量是——十七個。其總價通常能買下兩三座普氏族本部大樓。

按道理、將超一流獵人為了萬一之時的防護才持有的寶具、如此大量地時常裝備的男人，即使是在廣闊的帝都中也只有我一人吧。

當然，因為手指只有十根所以無法全部戴上，但放在道具袋中也同樣能發揮效果。說起來，如果沒有結界指也不會去使用『夜天之暗翼』這麼恐怖的寶具。

可是，這樣也並非是無敵的。

結界指的結界發動時間最長也只有一秒。也就是一瞬間。

發動一次後充能的魔力就會全被消耗掉，來這裡的途中與牆壁激烈碰撞的時候發動過幾次了，之後要是再從正面多承受幾次攻擊很快就會被打爆了吧。

在那之前不論如何都要逃掉。

說是有十七條命，但完全是言過其實。

「嘖……被躲開了」

露達叫了出來。如箭矢般射出的青色彈丸，被握著大劍的狼騎士迅速躲過了。

稍稍低下身子躲過了瞄準著頭部的彈丸。青色的彈丸從狼騎士頭上掠過後——轟鳴聲在洞窟中震蕩著。

「！？」

狼騎士被強烈的衝擊砸向地面。剛剛飛過去的彈丸緊接著又飛了回來，命中了它的後頭部。

狼騎士們動搖了。我一邊緊盯著它們一邊叫道。

「緹諾，跑起來！」

「！？是、是的」

緹諾和吉爾貝特少年他們如子彈般跑了起來。
狼騎士們只是注視著，並沒有追擊。

彈指是能釋放出魔法彈丸的寶具的總稱。

『驅動衝擊之戒指』在最大充能狀態時是能夠放出七發子彈的彈指，而且每一發子彈在擊中對手時都會釋放出強大的衝擊。

而且，令人無奈的是，從花哨的外觀看來也不像是有什麼威力。現在腦袋與地面親密接觸趴著的狼騎士也稍微被嚇到了吧。
雖說彈指的種類很豐富，但也不是能打倒幻影的強力物品，只能用作牽制。

趴著的狼騎士雙手撐地慢慢站了起來。如預想一樣，並沒有什麼明顯的外傷。

狼騎士們以我為中心，擺出了扇形的陣勢。前衛兩體、後衛兩體，真是不錯的平衡。

那個槍械有點糟糕吧？連射攻擊可是最差的相性來的？

明明之前很難得地在害怕著希特莉史萊姆，但似乎遭到反擊激怒了它們。
眼神中看到的是害怕占一成、憤怒三成、怨恨三成、以及警戒三成嗎（適當地）。

首先要考慮的是盡可能為緹諾爭取逃走的時間。我的話最差情況再飛一次就好。

掌握著武器的話多少都能牽制住對手吧。

我一邊嘿嘿地笑著，一邊從背著的鞘中將劍型寶具拔出——這時候，手抓空了。

不管再抓幾次，手碰到的只有十字弓型的寶具——持有著彈道操作能力、可以操作之後再也不會輕易出現的人型導彈和、魔法彈丸軌道的『絕對彈丸命中君』（命名者是並不具有絕對命中能力的我）。

「不會吧……丟了嗎？」

雖然還有鞘但裡面的東西卻沒了。

試著回憶途中發生的事，但當時心思一直都在拚命避免撞牆上，不知何時弄丟的。明明很貴的。
嘛、反正也不具有打破現狀的能力就是了……

狼騎士們警戒著我意義不明的動作。

「master！？在做什——」

應該正在逃走的緹諾在道路入口處看著我。
不僅僅是緹諾，其他的成員似乎也在等著我。都說了讓你們快跑了吧。


剛剛的動作是要做什麼，我自己也想知道呢。我是要幹什麼來著？
在寶物殿裡把寶具弄丟了，我這是在幹什麼啊？都不能說是運氣差。就只是耍笨吧？

……只是個笨蛋罷了。

沒辦法，我做好了覺悟。就這樣玉石俱焚吧。

「真是沒轍，只有這個是本不想使用的。」

半是自暴自棄地、我將脖子上掛著的食指大小的金屬膠囊摘了下來。

狼騎士們睜大了眼睛，忽然後退了數步。
果然這群傢伙害怕的，並不是我而是我手中的這個啊。就知道是這樣。

橫豎都要死的話那全員都被希特莉醬製作出的這個糟糕透頂的、史萊姆？吞掉也不錯。具體不知道是個什麼東西，也不想知道。

我用因緊張而顫抖的指尖拔掉瓶蓋，在扔出去前瞅了瞅膠囊裡面。


「……」


揉了揉眼睛再次確認。然後皺起眉，小心翼翼地試著用食指戳進裡面。
緹諾她們用擔心的表情看著我。

我大大地點了點頭，然後重新蓋緊瓶蓋。

就那樣高高地舉過頭頂，向狼騎士們所在的方向扔了出去。同時以之為目標放出了衝擊彈。

狼騎士們叫嚷著以敏捷的動作大大地拉開了距離。
我確認到被施以彈道操作的魔法彈丸命中膠囊的同時，向緹諾那邊跑了出去。

「緹諾，趕快！」

看到跑過來的我，緹諾她們一齊動了起來。

金屬膠囊裂開了。
狼騎士發出了施壓般的咆哮，但我沒空理它們。

趁它們還沒注意到金屬膠囊是空的，要趕快逃。

……怎麼回事兒啊？裡面的東西到哪去了？可怕。