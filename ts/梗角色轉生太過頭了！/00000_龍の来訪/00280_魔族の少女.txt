越過敵陣後，娜哈特穿過了肯帕爾尼亞草原。

舖修的道路中斷，粗獷的地面稍微綿延後綠色的色調慢慢增加，廣大的各種樹茂密地排成行的森林映入視野中。
雖然也是不久前娜哈特降生的地方，卻有種相當懷念的感覺。
娜哈特落在大地上，折起消去了展開的翼。
儘管塵土飛揚，但那些沒有弄髒娜哈特。飛舞在空中的飛塵被身上穿的暗之衣吞沒後消失了。

娜哈特用龍眼遠望森林深處。

作為四次職龍姫的被動技能（Passive Skill）的龍眼因為有選定之瞳這一別名所以在遊戲時代是因攻擊範圍增加的能力而被人珍視的技能（Skill）。雖然除此之外也有著看破、危險感知、回避率增加等複數的效果，但最承蒙關照的能力果然還是魔法的射程範圍增加吧。雖然如今抱著即使眼睛變好攻擊範圍也不會伸長吧的感想，但在娜哈特的心中有著因為是遊戲所以就是這樣的認識。儘管在解說文上是被描述為將映在視野中的一切作為攻擊對象選定的魔眼，但實際上視野之類的曖昧的要素很淡薄，在戰鬥中好好地存在著射程距離。即使現在也覺得那是在沒用的地方公平的遊戲。

娜哈特能正確制御強大魔法的原因就是限定攻擊對象的魔眼力量很大。以前落在貴族公館的竜也是通過魔眼而受到制御。

現在的娜哈特最大限度地受到了被強化的龍眼的恩惠。周圍的各種樹像透明的一樣映在視野中，只有想找的人會映在視野中。直到地平線的彼方為止，正確的風景映入眼中。大概是受到了視力的強化和看破的恩惠的影響吧。

「真是相當自在吶」

娜哈特嘟噥了一句。
那樣說的同時，從背後感覺到了比較大的魔力的波動。在那裡也能感覺到娜哈特熟悉的波動。

「那邊好像也開始了吶」

艾夏憑自己的意思選擇站在和娜哈特不同的戰場。
當然娜哈特往死裡反對了，但對堅決不改變想法的艾夏是娜哈特先讓步了。最後，將令人驚嘆的道具交給了艾夏後，在和克麗絲塔的隊伍一起行動的前提下同意了。
即使離開了，唯獨自己的從者放出的魔力也能靠肌膚感覺到。
娜哈特對那樣的感覺露出了笑容。
艾夏好像超出娜哈特想像的成長了。

「這邊也開始吧──」

娜哈特起手選擇創造出戰場。
在森林深處優雅地享受喝茶時間來奢侈的敵人的身姿映入了眼中。

風魔法（Wind Magic）──橫掃暴風（Tempest）

刮過的風就是風暴其本身──不，足足超過了自然發生的風的極限吧。
在地裡根深蒂固的各種樹被從大地剝離，和土塊一起被乘風吹飛。
像沙石滑坡或雪崩一樣的光景在眼前展現。不過，和沙石滑坡不同，那些不是向下而是向正側面被吹飛。

混在壓倒的風壓中的是不可見的刃。
如果是普通的人類的話會被吹飛壓死，或者被風之刃切得七零八落吧。
雖然對娜哈特來說就是戰鬥前的小練習，可造成的結果是４～５千米圈內的地形變化了。
綠色洋溢的森林一瞬間就落得了光禿禿的荒地。
填滿視野的飛塵被風捲起，看起來就像沙塵暴一樣。

但是，突然──銀線穿過。
一閃切裂了翻卷的沙塵暴，瞬間沙塵放晴了。
一位少女無傷站在那裡。
娜哈特的表情染上了驚愕。
不過那絶不是因為少女從娜哈特放出的魔法下無傷生還了。

被少女握著的是一柄長劍。
單手持的長劍有著像將水晶鍛鍊了的色調。接近透明的青色劍身和被刻在劍柄上的帶刺的荊棘紋章。左手上是像籠手一樣的白銀的盾。如鏡子般照射光的盾下也有形象化了被荊棘包圍的薔薇花的紋章。

娜哈特認得那些。
長著鮮艷的角的少女對吃驚的娜哈特開口道。

「沒禮貌的人──對待淑女的方式真不像話呢」
「呋姆，我面前只有個矮個兒的小孩吶。看不到有淑女什麼的？」
「啊啦是嗎，那對美麗的眼睛好像是光有外表的裝飾呢」
「不湊巧我的眼睛只會映出真實──」

那樣說著娜哈特的視線稍稍往下了。

「──有不滿的話，等那寒酸的胸部大上兩圈之後再說」
「────！」

雖然是一瞬間但豪放的少女怯陣了。
娜哈特沒有天真到會放過那個空子。

「被說中了嗎？就算是我家的艾夏也還再稍微大點哦？即使貧乳是狀態，無乳也是連評價都不值吶」

少女的臉染得通紅。
那有九成以上是出於憤怒吧。握著劍的手在抖，散發著馬上就要砍上去似的氣氛。

「你，這傢伙──不要亂說！我才七十二歳！是孩子！和你這樣的不同還有成長的餘地！啊，但是，加上沉睡的時間的話──嗯嗯，不對。一定接下來才是動真格的！」

對簡單崩壊了的腔調娜哈特沒有吃驚，甚至理解了。
雖然銳利的眼睛也夾雜著憤怒正放出著敵意，但那就是像艾夏一樣的孩子般的眼睛。不如說，也有本人說了是孩子的事實在。

「那麼，姑且作為禮儀也對小孩子報上姓名吧。我叫娜哈特。娜哈特・夏坦。飽含敬意地叫我娜哈特醬也可以哦？」
「不許叫我小孩子！聽好，聽仔細了，同愚蠢的人類聯合的人喲！我的名字是莉諾亞・路提納・格里摩爾。我是格里摩爾公爵家的長女，是作為魔王大人血統的真正的魔族！低下頭，跪拜吧！」

娜哈特對高聲宣言的莉諾亞送去了從心底瞧不起的目光。

「好好，做好了自我介紹好偉大呢」
「不—要—把—我當孩子！」
「雖然對長輩的腔調稍微有點不像話，不過因為是孩子就寬恕了吧。那麼，莉諾亞什麼的。你最好現在馬上停止軍隊遊戲。從這個地方離開」

娜哈特像指責一樣斷言道。
莉諾亞的眼睛再次變銳利後瞪了娜哈特。

「你在說什麼？夢話──」

睡著之後再說，對想要這樣說的莉諾亞娜哈特插嘴告訴道。

「不明白嗎？那麼我再說清楚點吧。我說如果老實收手的話，我就作為孩子的惡作劇寬恕你」

對娜哈特的忠告，莉諾亞只是顯露了更加激動的情緒。

「開什麼玩笑！誰會停止啊！人類（垃圾）們如果投降的話我還能饒你們一命。我和你們人類不同有同情心。老實向我投降──」

看到像是被憎恨左右的眼睛，娜哈特歪了頭。

「你為什麼憎恨人？為什麼要襲擊那個城鎮？為什麼，你會露出那麼悲傷的眼神？」

莉諾亞深吸了一口氣。
小小的驚訝像被強大的怒氣壓碎一樣消去了。

「為什麼？你們說這個？拒絶和睦，給了我們仇敵這個記號（帽子），並將那詐稱為聖戰的你們人類，將憎恨關在裡面安穩地生活的你們人類，事到如今還說什麼！」
「呋姆，雖然好像有各種各樣的誤解和原因，但我再警告你一次吧。收手」

娜哈特不認為眼前的少女稍微有些沾污。
決看不出作為傳說或常識充斥世間的仿彿惡魔的印象。儘管艾夏很害怕，但娜哈特不認為她是會把壊孩子拐走從頭吃掉這樣的愚蠢傳承的對象。
所以，娜哈特那樣告訴莉諾亞。

「我拒絶！不過，我反過來警告你。老老實實向我投降。那樣的話我不光會保障你的生命，還會讓你成為我的部下。畢竟剛才的魔法也挺厲害的呢」

沒有動搖的強烈意思帶著被憎恨形象化了的顏色。
娜哈特無可奈何的嘆氣。
不由得感覺只靠談話的話好像完全就是單方面傳達意見。

「無論是魔王的血緣也好，水晶樹荊棘的套裝也好，雖然有各種各樣想問的事──但為了讓你老老實實聽話首先管教一下吧」

娜哈特和威壓一起斷言道。

「啊啦，對我的裝備感興趣嗎？」
「啊啊，之後會問的」
「是嗎，不後退呢──」

少女重新架起刀刃。
用盾的銀鏡反射陽光，採取了低身突進的架勢的少女的嘴微微張開了──

剎那。

那個不知是從哪裡像影子一樣出現了。
即使擁有娜哈特的知覺，也連氣息出現都沒能察知。
在不滿零點一秒的思考期間微弱的疑問和驚愕同時出現了。

看到的時候有把從背後向喉嚨迫來的刀刃。
只能說櫻色的頭髮像沒有實體的幻影一樣出現了。那個毫不留情地插上銳利的刀劍。

──真遺憾啊。

莉諾亞那樣有氣無力地嘟囔了。