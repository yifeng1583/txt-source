我和佩特羅、梅亞一起，乘坐小型木偶龍月神，向利維拉絲国的聖都利維奧林進發。

小型木偶龍月神是木偶龍羽蛇神的替代品，由我指揮建造的運輸用歐特魯。全長約七米，整體是一頭龍的姿態。木偶龍羽蛇神的設計是以紅色為基調的太陽的形象，而小型木偶龍月神則是以青色為基調的月亮的形象。

木偶龍的胸部嵌入了刻畫著魔術式的球形魔礦石塊，其產生強力的浮力，使木偶龍能在空中飛行。雖然有配置雙翼，但幾乎只是裝飾，就算沒了翼也不會動不了。另外，魔礦石塊展開的結界能擋住迎面的風，在木偶龍加速、減速時也能產生力場抵消慣性。

佩特羅一副生無可戀的眼神俯視著遙遠的下方，發出嘆息。

「⋯⋯亞貝魯醬，這次是情況特殊沒有辦法，但是，使用轉移魔術、格雷姆或者龍等大型移動手段去往国外本來是最大的禁忌，所以我不在場的時候請不要這麼做呢」
「也就是說，每次要去的時候帶上佩特羅先生就沒問題了是吧？」
「可以的話希望這次就是最後一次呢」

不過，佩特羅所說的應該不會實現。根據他所說，四大創造神的国家被酋雷姆伯爵唆使，被誘導向迪恩那多王国發出攻擊。儘管現在還沒有公開化，但事態加深之際，與加爾沙德王国、瑪哈拉努王国、甚至是高等精靈的浮游大陸天空之国（阿爾芙海姆）的關係會惡化，小規模的衝突會開始發生。今後，像這樣的情況還會增加。

「我已經，很累了呢。⋯⋯要不要把一切都交給亞貝魯醬然後引退呢⋯⋯」
「繆西小姐會哭的噢。而且我對政治什麼的一竅不通，不想牽涉進這麼麻煩的事情裡呢」
「亞貝魯醬的話全部都強行鎮壓就能解決了吧。要是薩戴利亞知道了只是亞貝魯醬一個人就奪回了幾乎被完全佔領的琺基領，還把三大幹部抓起來關進監獄裡，她可是會哭的呢」

佩特羅一邊嘆息一邊說道。

說起來，就在前幾天我不在領地的期間，利維教的刺客拉斯布德過來襲擊了。今天也同樣，為了避免發生預料之外的事情而不知所措，佩特羅與我同行。梅亞也是為了避免被拉斯布德那樣的傢伙襲擊也一起帶了過來，但把她帶到敵人陣地的正中真的是正確的選擇嗎，我無論如何也說不清楚。

沉默了一陣之後，佩特羅再次開口。

「這次的目的是，教皇薩戴利亞的誘拐，以及利維的討伐喲。你明白嗎？」
「⋯⋯」

再次聽到這個，還真是件大事啊。也許會出現大量的死傷者，失去利維以後，利維拉絲国也會結束統一，各教派為爭奪聖地的主權會讓矛盾激化，佩特羅是如此預測的。利維教的分教，算上較小的那些，數量不止十幾二十几。

「我不是在危言聳聽，亞貝魯醬如果不這麼做，以後發生戰爭的時候會產生更多的死者，這結果和你選擇去做沒有什麼區別。那個国家已經失去控制了，明明在窘困中還要持續對這邊出手，薩戴利亞毫無疑問是個笨蛋呢。但問題的源頭並不僅僅是薩戴利亞和利維，只要狂熱崇拜龍脈的血液依然流淌，問題就永遠不止呢」
「⋯⋯我想盡量謹慎行動。雖然你說教皇薩戴利亞在聖都利維奧林的宮殿裡，但她可能會因為什麼要事出外，這你有沒有考慮過？」

假如薩戴利亞不在聖都利維奧林里，那這次的襲擊本身會變得毫無意義。

「那種事不會發生呢。雖說只是根據利維拉絲国的偵察情報，但至少在這兩年間，薩戴利亞別說是聖都，連宮殿都不曾離開，而且聖都的圍牆高聳，位於中央位置的薩戴利亞的宮殿被巨大的水道環繞，出入並不便利噢。雖然立有橋樑，但幾乎沒有被使用過呢」

佩特羅展開地圖說明起來。安靜地聽著我和佩特羅討論的梅亞，也把頭從旁邊伸過來看著地圖。

「⋯⋯一次都沒有嗎？真是個相當執著的阿宅呢」
「哎哎，你知道其中的原因嗎？」

是為了守護聖都利維奧林的龍脈吧。薩戴利亞以外的人應該無法引出龍脈的魔力。龍脈對於她來說是盾牌，她對於龍脈來說也是武器吧。

聖都若是沒有了薩戴利亞，其他的教派肯定會趁機攻入奪取。然後薩戴利亞她自己也是，不在聖都的期間是無防備的狀態，可以想像到會受到其他教派的人襲擊。

「⋯⋯薩戴利亞一直待在宮殿的話襲擊起來很簡單這很有幫助，但這樣的話，她不是一輩子都不能離開宮殿了麼。而且，要是沒有其他繼任者的話，国內的管理不會荒廢嗎？」
「說得也是呢。在這一點上，真是個被詛咒的国家，和平安穩是永遠不可求的事物呢。我們能的事就只有稍作干涉，讓他們的腦子確實地理解到不可以惹惱我們這邊呢」

佩特羅有點消極地說道。他雖然說只要我強力鎮壓就能萬事解決，但這個世上有無法用力量強行解決的事情。例如庫多爾要是想的話大概就能毀滅利維拉絲国，但要是想讓這個国家變得和平，也許誰都不可能做到。

遠處隱約開始見到聖都利維奧林。那是被高大牆壁圍住的城市，中央有著巨大水道環繞的土地，在那之上建立著一座高聳巨大的塔。那就是薩戴利亞所在的宮殿吧。

與水神利維的決戰時刻，終於到來了。