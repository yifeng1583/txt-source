１０

「是這麼一回事。」
「原來如此吶。無法反駁的東西嗎。」
「喂。教我〈回復〉阿。〈回復〉。」
「話說，有這種東西來著。能用嗎？」

雷肯從〈收納〉取出了形狀奇妙的魔導具。是能擊出炎之魔法的武器。

「從哪弄來的阿。」
「話還沒說完嗎。趕快來練習吧。」
「最初護衛傑尼的時候，從襲擊的人身上搶來的。」
「原來如此。這東西能用呢。」
「〈回復〉嗎。真想不到會有能用的一天，連夢都沒夢過呢。」
「米多斯可說這個壊掉了的樣子是吧。」
「是阿。強硬地主張，因為是不良品，所以一測試就壊成碎片了。但是要他把碎片拿來，卻說那種東西早就丟掉了。」
「冒険者能使用〈回復〉，那可是超受歡迎的阿。」
「能證明這是那東西嗎？」
「各個都有被刻上號碼喔。你看，在這裡。是我給領主的其中之一沒錯。要是在米多斯可的房屋裡發現的話，會怎麼樣呢。」
「回，復。回，復。」
「領主花了大錢買來的最強力的武器，硬是拿去保管卻弄壊了，光是這樣就已經算重罪了吧。」
「是阿。在此之上，要是壊掉其實是謊話，真相是偷藏起來的話。」
「話說回來，我使用魔法時都沒有用魔杖，沒關係嗎。」
「會被問以謀反罪吧。」
「小艾達。魔杖是便利的東西喔。構築魔法會變輕鬆，也能引出強大的力量。但是太常用魔杖的話，魔法的技術就不容易有所成長，魔力也難以增加。在成長途中的魔法使呢，如果不需魔杖就能使用魔法，那就別用比較好。總有一天會找到適合的魔杖給你的。」
「謝謝。」
「雷肯。我有點事情要辦，你先教教艾達〈回復〉吧。」
「太好啦。」
「不。在那之前先去冒険者協會。你也要。」
「為什麼啦。」
「你，真的沒在聽人說話阿。」


１１

「恭喜達成委託。報酬請直接跟委託人領收。此外，加上這次的功績後，雷肯先生升格為銀級了。明天請來領取冒険者徽章。」
「誒。我呢？我不能升格嗎？」
「不能。」
「怎麼會。艾拉。不對吧。我達成的委託量，多得雷肯根本比不上才對啊。」
「因為初期的連續失敗影響很大。而雷肯先生的委託達成率是百分之百，而且全都拿了高評價。」
「嗚嗚。不能想想辦法嗎。」
「沒有辦法的。話說回來，雷肯先生，孤兒院提出了指名奉仕委託。似乎還想請您陪孩子們玩的樣子。」
「我拒絶。」


１２

「歡迎回來。」
「我回來了。」
「那件事如何？」
「都準備好了喔。之後就看領主怎麼做了。」
「是嗎。」
「那麼，雷肯。來給艾達第一回的〈回復〉傳授吧。」
「雷肯師傅！拜託你了。」

雷肯思索著。
就算說明自己的學習方式，艾達也絶對無法理解。
要以更直觀，更實踐性的方法才行。
妮姫說過，發動的關鍵在於祈禱有多深沉。
既然如此。

「艾達。」
「怎樣。」
「假設你重要的人，在這裡。」
「重，重要的人，什麼啊。沒那種人啦。」
「母親、父親、兄弟姊妹、或是交情好的朋友。就算是已經過世的人也行。」
「啊啊？阿，什麼嘛，那種意思啊。那，爸爸，吧。」
「你父親正受了傷。」
「誒誒。」
「受了傷，很痛。很痛苦。但是沒有人能幫助他。」
「為，為什麼阿。為什麼沒人能幫阿。」
「沒有人能幫助他。事情就是這樣。」
「沒，沒什麼辦法嗎。」
「有方法。」
「拜託告訴我吧。」
「把雙手伸出來。」
「這，這樣嗎？」
「把雙手合起，稍微捧著。像是要接住落下的細雪。」
「接住什麼？」
「像是要捧水。」
「喔，喔。這種感覺嗎。」
「從你的手掌中，形成了溫暖的光。」
「光？」
「你的思念轉變成了光。」
「聽，聽不懂啊。」
「不需要懂。去感覺。」
「感覺？」
「如果你想要拯救父親的思念是真的，光就會形成而出。」
「真，真的嗎？」
「柔和的，能滿溢一切的，讓人幸福的光。」
「幸福的⋯⋯光。」
「只要打從心底想治癒父親的傷，那思念就會化為光。」
「思念會⋯⋯化為光。」
「然後，詠唱出來。〈賜予治癒〉。」
「奇，奇利姆。」

啵，的聲音發出，艾達彎圓的手中，產生了一道溫暖的光。
那是給予看著的人的心，懷念與安詳的，治癒之光。

「做，做到了！做到了喔。雷肯！」

艾達開心地流著淚。

雷肯驚訝地睜大了眼。
雷肯自己在三天裡，接受著妮姫片刻不離的指導，不斷不斷重複挑戰，最後才終於亮起小小的光。

而艾達僅僅一次，而且還是雷肯馬馬虎虎的指導，就成功了。那光芒的大小，遠比雷肯最初成功時的還大。

雷肯抱著稍微得意的心情，看向妮姫。
妮姫擺著非常嚴肅的臉。